sweethome3d-textures-editor (2.1-1) unstable; urgency=medium

  * New upstream version 2.1.
  * Declare compliance with Debian Policy 4.7.0.
  * Update debian/copyright for new release.

 -- Markus Koschany <apo@debian.org>  Sun, 09 Feb 2025 21:09:24 +0100

sweethome3d-textures-editor (1.8-1) unstable; urgency=medium

  * New upstream version 1.8.
  * Declare compliance with Debian Policy 4.6.0.
  * Refresh build.patch.

 -- Markus Koschany <apo@debian.org>  Wed, 06 Oct 2021 14:19:12 +0200

sweethome3d-textures-editor (1.6-3) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.

 -- Markus Koschany <apo@debian.org>  Wed, 06 Jan 2021 01:51:41 +0100

sweethome3d-textures-editor (1.6-2) unstable; urgency=medium

  * Depend on icedtea-netx instead of icedtea-netx-common. (Closes: #924605)
    - Thank you to Matthias Klose for the bug report.

 -- tony mancill <tmancill@debian.org>  Fri, 22 Mar 2019 21:55:57 -0700

sweethome3d-textures-editor (1.6-1) unstable; urgency=medium

  * New upstream version 1.6.
  * Switch to compat level 12.
  * Declare compliance with Debian Policy 4.3.0.
  * Use canonical VCS URI.

 -- Markus Koschany <apo@debian.org>  Mon, 28 Jan 2019 18:16:32 +0100

sweethome3d-textures-editor (1.5-2) unstable; urgency=medium

  [ tony mancill ]
  * Remove Gabriele Giacone <1o5g4r8o@gmail.com> from Uploaders.
    (Closes: #856775)

  [ Markus Koschany ]
  * Tighten build-dependency of sweethome3d to >= 5.5.
  * Require Java 8 from now on.
  * Declare compliance with Debian Policy 4.1.0.
  * Add myself to Uploaders.
  * wrap-and-sort -sa.
  * Drop deprecated menu file and xpm icon.

 -- Markus Koschany <apo@debian.org>  Thu, 14 Sep 2017 07:59:34 +0200

sweethome3d-textures-editor (1.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.5.
  * Switch to compat level 10.
  * Depend on alternative java7-runtime.
  * Remove override for dh_builddeb because xz is the default now.
  * sweethome3d-textures-editor.desktop: Add a comment in German.
  * sweethome3d-textures-editor.sh: Use batik-all instead of batik-parser.
    Remove java3ds-fileloader because it is not needed.
  * Rebase 00build patch and rename it to build.patch
  * Declare compliance with Debian Policy 3.9.8.
  * Vcs: Switch to cgit and https.
  * Update debian/copyright.
  * Depend on libvecmath-java, libbatik-java and libjava3-java.

 -- Markus Koschany <apo@debian.org>  Mon, 10 Oct 2016 14:37:01 +0200

sweethome3d-textures-editor (1.4-1) unstable; urgency=medium

  * New upstream release.
  * Build-Depends on sweethome3d 4.4.
  * Bump Standards-Version to 3.9.6 (no changes).

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sat, 08 Nov 2014 02:44:20 +0100

sweethome3d-textures-editor (1.3-1) unstable; urgency=low

  * New upstream release.
  * Build-Depends on sweethome3d 4.2.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Wed, 06 Nov 2013 00:34:52 +0100

sweethome3d-textures-editor (1.2-1) unstable; urgency=low

  * New upstream release.
  * Fix icons gamma correction.
  * Bump Standards-Version to 3.9.4 (no changes).
  * Canonical URI in Vcs-* fields.
  * Add Keywords to desktop file.
  * Depend on sweethome3d 4.1.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Mon, 26 Aug 2013 20:08:04 +0200

sweethome3d-textures-editor (1.1-1) unstable; urgency=low

  * New upstream release.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sat, 23 Mar 2013 01:21:21 +0100

sweethome3d-textures-editor (1.0-1) unstable; urgency=low

  * Initial release (Closes: #691695).

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Fri, 28 Sep 2012 23:00:05 +0200
