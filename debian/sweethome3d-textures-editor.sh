#!/bin/sh
#
#
BASEPATH=/usr/share/sweethome3d
JAVA_ARGS="-Djava.library.path=/usr/lib/jni"

. /usr/lib/java-wrappers/java-wrappers.sh

find_java_runtime java8

find_jars j3dcore j3dutils vecmath batik-all
find_jars /usr/share/icedtea-web/netx.jar
find_jars /usr/share/sweethome3d/sweethome3d-textures-editor.jar

cd $BASEPATH
run_java com.eteks.textureslibraryeditor.TexturesLibraryEditorBootstrap "$@"

