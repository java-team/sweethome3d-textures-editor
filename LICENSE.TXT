Textures Library Editor, Copyright (c) 2024 Space Mushrooms

This software is licensed by Space Mushrooms under GNU General Public License.
Please, read COPYING.TXT file for more details about GNU General Public License.


This software includes also materials developed by third parties.
Please, read the files named THIRDPARTY-LICENSE for more details about third parties materials licenses.
